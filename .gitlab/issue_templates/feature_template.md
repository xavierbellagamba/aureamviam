## What
Briefly explain what the ticket is about

## Why
Briefly explain why the ticket is important and what benefits it brings

## How
Provide how it going to be implemented: dataset used, algorithm used, newly created files, modified files (and potentially which classes or funcitons are impacted). 

### Newly created files
- Which new files are being created as part of this new ticket

### Modified files
- Which files are being modified as part of this ticket

### Datasets
- Provide a description of the datasets to be used

## Critical dependencies
- List the critical dependencies to be resolved to start working on this ticket

## Acceptance criteria
- [ ] List the different criteria that need to be fulfilled to accept this ticket as closed

