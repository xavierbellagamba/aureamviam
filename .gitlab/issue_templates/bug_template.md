## What's wrong
Briefly explain what the ticket is about

## What needs to be changed
Briefly explain what needs to be done to solve the problem

## Critical dependencies
- List the critical dependencies to be resolved to start working on this ticket

## Acceptance criteria
- [ ] List the different criteria that need to be fulfilled to accept this ticket as closed
